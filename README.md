#	MapboxGL - Clustering/Heatmap

__INTRODUCTION__: This project is for a Sisense plugin, which generates a new widget type for MapboxGL maps.  This map takes in data points, in Latitude and Longitude format, and automatically groups them for display when zoomed out.

![Widget Editor](screenshots/widget-editor.png)

__IMPLEMENTATION__: 

*Step 1:* Download the attached .zip file, and extract it to the following path "[Sisense installation path]\PrismWeb\Plugins". 

*Step 2:* Open the included file, *config.js*, and add your mapbox API token in the *apiToken* setting.  For more information on how to obtain a mapbox token, please use the following links

https://www.mapbox.com/help/how-access-tokens-work/

https://www.mapbox.com/api-documentation/#tokens

*Step 3:* On your dashboard, click the *Create Widget* button and select *Advanced Configuration*. Next, select *Mapbox GL* from the dropdown.  In the data tab, there are several panels for adding data.

* Latitude(required): The latitude field for each data point (must be a number)
* Longitude(required): The longitude field for each data point (must be a number)
* Label: If used, this is the label that will show up within the tooltip for each point
* Value(1 required): One or more measures to calculate per point.  The first value added is also used to determine the coloring of the points

*Step 4:* Coloring
When using the cluster option, the cluster colors will come from the dashboard's color palette.  The defaults here are to color the clusters based on 0-100, 100-750, and 750+ points.  This can be adjusted in the widget.js file by modifying the getLayerCluster function.  

When displaying heatmaps, the coloring is derived from the first value's color selection.  
* Single color: the heatmap will range from white -> your selection
* Range: The heatmap will generate a range of colors between the min/max
* Conditional Colors: The heatmap will range between these colors, using the first condition as the least dense color and the last condition as the most dense.
![Coloring](screenshots/heatmap-coloring.png) 

*Step 5:* Formatting

* Basemap: This is the background to use for each widget.  We've included the default public map styles, but you can add to this by editing the *config.js* file.  For more information on creating your own basemaps, check out Mapbox's documentation site
https://www.mapbox.com/help/create-a-custom-style/
* Style: This is the grouping setting, can be clustering or heatmap.  If Clustering is selected, the widget will generate larger cirles to group the points.  For heatmap, the points are blended together so they don't show up overlapping.  Either way, zooming into the map will eventually show the specific data points
* Zoom Level Threshold: This slider determines how far you have to zoom in, before viewing the raw data points
* Heatmap Intensity: When displaying a heatmap, how much should the # of points skew the color?
* Tooltip: Determines how the tooltips are triggered
* 
![Tooltip](screenshots/tooltip.png)

*Step 6:* Using the widget
* You can drag directly on map to pan across your data
* Double clicking on the map zooms in, and you can also use the scroll wheel of your mouse
* In order to make filter selections, hold down the shift key and draw a shape on the map.  Once selected, a filter will be added to the dashboard for the Latitude and Longitude

![Making a filter selection](screenshots/filter-selection1.png)

![After selecting a shape](screenshots/filter-selection2.png)

* You can reset any filter selections by clicking the "Reset" buttom at the top left of the chart

__NOTES__: 
* This sample has been confirmed working on Sisense version 7.0.1, and should be backwards compatible with previous version.
* There is a known limitation with this plugin, as it does not support export to PDF