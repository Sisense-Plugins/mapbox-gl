
var settings = {
	'apiToken': '<MAPBOX-ACCESS-TOKEN>',
	'basemaps': [
		{
			'key': 'basic',
			'value': 'mapbox://styles/mapbox/basic-v9',
			'bottomLayer': 'place_label_other'
		},
		{
			'key': 'streets',
			'value': 'mapbox://styles/mapbox/streets-v9',
			'bottomLayer': 'waterway-label'
		},
		{
			'key': 'bright',
			'value': 'mapbox://styles/mapbox/bright-v9',
			'bottomLayer': 'water-label'
		},
		{
			'key': 'light',
			'value': 'mapbox://styles/mapbox/light-v9',
			'bottomLayer': 'waterway-label'
		},
		{
			'key': 'dark',
			'value': 'mapbox://styles/mapbox/dark-v9',
			'bottomLayer': 'waterway-label'
		},
		{
			'key': 'satellite',
			'value': 'mapbox://styles/mapbox/satellite-streets-v9',
			'bottomLayer': 'waterway-label'
		}
	],
	'animation': false,
	'fitPadding': 20,
	'zoom': {
		'min': 0,
		'max': 24
	}
}