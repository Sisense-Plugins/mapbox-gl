mod.directive('mapboxglwidget', [

    function ($timeout, $dom) {

        return {
            priority: 0,
            replace: false,
            templateUrl: "/plugins/mapboxGl/template.html",
            transclude: false,
            restrict: 'E',
            link: function ($scope, element, attrs) {

                /************************************/
                /*  Map Formatting Handlers         */
                /************************************/

                //  Function for when the widget gets resized
                function mapResized(widget, args, scope){
                    
                    //  Look for the map object, corresponding to this widget
                    var map = $scope.map;
                    if (map) {
                        //  Resize the map
                        map.resize();
                    }
                }

                /************************************/
                /*  Functions for point selection   */
                /************************************/

                //  Function for setting/resetting a filter based on lat/lng
                function setFilter(bbox){
                    
                    //  Set the options
                    var latOptions = {
                        save:true, 
                        refresh:false, 
                        unionIfSameDimensionAndSameType:false
                    }
                    var lngOptions = {
                        save:true, 
                        refresh:true, 
                        unionIfSameDimensionAndSameType:false
                    }

                    //  Create filter objects
                    var latFilter = {
                        'jaql': $$.object.clone($scope.widget.metadata.panel(0).items[0].jaql,true)
                    };
                    var lngFilter = {
                        'jaql': $$.object.clone($scope.widget.metadata.panel(1).items[0].jaql,true)
                    };
                    
                    //  Did we get a bounding box?
                    if (bbox) {

                        //  Yes, figure out the start/end points (depends on direction of how the box was made)
                        var latStart = bbox[0].lat > bbox[1].lat ? bbox[1].lat : bbox[0].lat,
                            latEnd = bbox[0].lat > bbox[1].lat ? bbox[0].lat : bbox[1].lat,
                            lngStart = bbox[0].lng > bbox[1].lng ? bbox[1].lng : bbox[0].lng,
                            lngEnd = bbox[0].lng > bbox[1].lng ? bbox[0].lng : bbox[1].lng;

                        //  Update the filter objects
                        latFilter.jaql.collapsed = false;
                        latFilter.jaql.filter = {
                            "from": latStart,
                            "to": latEnd
                        }
                        lngFilter.jaql.collapsed = false;
                        lngFilter.jaql.filter = {
                            "from": lngStart,
                            "to": lngEnd
                        }
                    } else {
                        
                        //  No, reset the filters
                        latFilter.jaql.collapsed = true;
                        latFilter.jaql.filter = {
                            "all": true
                        }
                        lngFilter.jaql.collapsed = true;
                        lngFilter.jaql.filter = {
                            "all": true
                        }
                    }

                    //  Set the filters through the API
                    $scope.dashboard.filters.update(latFilter, latOptions)
                    $scope.dashboard.filters.update(lngFilter, lngOptions)
                }

                // Return the xy coordinates of the mouse position
                function mousePos(e) {
                    var canvas = $scope.selector.canvas;
                    var rect = canvas.getBoundingClientRect();
                    return new mapboxgl.Point(
                        e.clientX - rect.left - canvas.clientLeft,
                        e.clientY - rect.top - canvas.clientTop
                    );
                }

                //  Triggering function, a user clicked down on the map
                function mouseDown(event) {

                    //  Get the original event
                    var e = event.originalEvent;

                    // Continue the rest of the function, ONLY if the shiftkey is pressed.
                    if (!(e.shiftKey && e.button === 0)) return;

                    // Disable default drag zooming when the shift key is held down.
                    $scope.map.dragPan.disable();

                    // Call functions for the following events
                    $scope.map.on('mousemove', onMouseMove);
                    $scope.map.on('mouseup', onMouseUp);
                    $scope.map.on('keydown', onKeyDown);

                    // Capture the first xy coordinates
                    $scope.selector.start = mousePos(e);
                    $scope.selector.startPoint = event.lngLat;
                }

                //  Function that fires when the user moves his mouse (creating the box)
                function onMouseMove(event) {

                    //  Get the 
                    var e = event.originalEvent;

                    // Capture the ongoing xy coordinates
                    $scope.selector.currentPoint = event.lngLat;
                    $scope.selector.current = mousePos(e);

                    // Append the box element if it doesnt exist
                    if (!$scope.selector.box) {
                        $scope.selector.box = document.createElement('div');
                        $scope.selector.box.classList.add('boxdraw');
                        $scope.selector.canvas.appendChild($scope.selector.box);
                    }

                    //  Figure out the min and max
                    var minX = Math.min($scope.selector.start.x, $scope.selector.current.x),
                        maxX = Math.max($scope.selector.start.x, $scope.selector.current.x),
                        minY = Math.min($scope.selector.start.y, $scope.selector.current.y),
                        maxY = Math.max($scope.selector.start.y, $scope.selector.current.y);

                    // Adjust width and xy position of the box element ongoing
                    var pos = 'translate(' + minX + 'px,' + minY + 'px)';
                    $scope.selector.box.style.transform = pos;
                    $scope.selector.box.style.WebkitTransform = pos;
                    $scope.selector.box.style.width = maxX - minX + 'px';
                    $scope.selector.box.style.height = maxY - minY + 'px';
                }

                //  Function for when the user lets go of the mouse (done drawing box)
                function onMouseUp(event) {
                    // Capture xy coordinates
                    finish([$scope.selector.startPoint, event.lngLat]);
                }

                //  Cancel key was pressed, just finish
                function onKeyDown(e) {
                    // If the ESC key is pressed
                    var e = event.originalEvent;
                    if (e.keyCode === 27) finish();
                }

                //  Done drawing the box
                function finish(bbox) {

                    // Remove these event handlers now that finish has been called.
                    $scope.map.off('mousemove', onMouseMove);
                    $scope.map.off('keydown', onKeyDown);
                    $scope.map.off('mouseup', onMouseUp);

                    //  Reset the box
                    if ($scope.selector.box) {
                        $scope.selector.box.parentNode.removeChild($scope.selector.box);
                        $scope.selector.box = null;
                    }

                    // If bbox exists, Set a sisense filter, based on the bounding box
                    if (bbox) {
                        setFilter(bbox);
                    }

                    //  Enabled dragging on the map again
                    $scope.map.dragPan.enable();
                }

                //  Custom Mapbox Control for Resetting filters
                class clearFilterControl {
                  onAdd(map){

                    //  Set the map
                    this.map = map;
                    
                    //  Create HTML elements
                    var myButtonGroup = $('<div class="mapboxgl-ctrl mapboxgl-ctrl-group"></div>'),
                        myButton = $('<button class="clear-filter-button" type="button" aria-label="Reset Filters">Reset Filters</button>');

                    //  Add event handler
                    myButton.on('click',function(){
                        setFilter(null)
                    });

                    //  append button to group
                    myButtonGroup.append(myButton);

                    //  Assign as the container
                    this.container = myButtonGroup[0];

                    return this.container;
                  }
                  onRemove(){
                    this.container.parentNode.removeChild(this.container);
                    this.map = undefined;
                  }
                }

                /************************************/
                /*  Init the Map                    */
                /************************************/

                //  Get the widget id
                $scope.widgetId = $scope.widget.oid ? $scope.widget.oid : 'new';

                //  Wait until after the template has loaded
                setTimeout( function(){

                    //  Init the flag, so that the map is marked as "not loaded yet"
                    $scope.widget.options.mapLoaded = false;
                    
                    //  Define the access token for mpabox
                    mapboxgl.accessToken = settings.apiToken;

                    //  define the div container's id
                    var divId = 'mapboxgl-' + $scope.widgetId;

                    //  Define the 
                    var map = new mapboxgl.Map({
                        container: divId,
                        style: $scope.widget.style.basemap,
                        zoom: 2,
                        center: [-73.987697, 40.751759]
                    });

                    //  Set the flag within the widget, to mark the map as done initializing
                    map.on('load', function() {
                        
                        //  Add the standard navigation  control
                        var control = new mapboxgl.NavigationControl();
                        var controlOptions = {};
                        map.addControl(control, $scope.widget.style.controlLocation);

                        //  Add custom control for resetting filters
                        var resetControl = new clearFilterControl();
                        map.addControl(resetControl, 'top-left');

                        //  Save the control to the widget
                        prism.mapboxglwidget[$scope.widgetId].control = control;
                        prism.mapboxglwidget[$scope.widgetId].resetControl = resetControl;

                        //  Init variables for point selection
                        $scope.selector = {
                            start: null,    //  Start mouse location
                            current: null,  //  Current mouse location
                            box: null,      //  Bounding box
                            canvas: map.getCanvasContainer()    //  map canvas container
                        }
                        $scope.map = map;

                        //  Add handler for point selection
                        map.on('mousedown', mouseDown);

                        //  Mark widget loaded flag
                        $scope.widget.options.mapLoaded = true;

                        //  Add handler for
                        $scope.widget.off('readjust', mapResized)
                        $scope.widget.on('readjust', mapResized)
                    })

                    //  Save a reference to the map object, within the widget's options
                    if (typeof prism.mapboxglwidget === "undefined") {
                        prism.mapboxglwidget = {}
                    }
                    prism.mapboxglwidget[$scope.widgetId] = {
                        map: map,
                        control: null
                    };
    
                },0)
            }
        }
    }]);

