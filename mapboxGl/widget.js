prism.registerWidget("mapboxglwidget", {
    name: "mapboxglwidget",
    family: "map",
    title: "Mapbox GL",
    iconSmall: "/plugins/mapboxGl/icon-24.png",
    styleEditorTemplate: "/plugins/mapboxGl/styler.html",
    hideNoResults: true,
    style: {
        basemap: "mapbox://styles/mapbox/basic-v9",
        basemapLayer: "housenum-label",
        renderType: "heatmap",
        showControl: true,
        controlLocation: "top-right",
        tooltipEvent: 'click',
        clusterLabelMethod: 'points',
        animation: false,
        zoomLevel: 2,
        intensity: 5
    },
    directive: {
        desktop: "mapboxglwidget"
    },
    data: {

        selection: [],
        defaultQueryResult: {},
        panels: [
            {
                name: 'Latitude',
                type: 'visible',
                metadata: {
                    types: ['dimensions'],
                    maxitems: 1
                },
                visibility: true
            },
            {
                name: 'Longitude',
                type: 'visible',
                metadata: {
                    types: ['dimensions'],
                    maxitems: 1
                },
                visibility: true
            },
            {
                name: 'Label',
                type: 'visible',
                metadata: {
                    types: ['dimensions'],
                    maxitems: 1
                },
                visibility: true
            },
            {
                name: 'Value',
                type: "visible",
                metadata: {
                    types: ['measures'],
                    maxitems: 5
                },
                itemAttributes: ["color"],
                allowedColoringTypes: {
                    color: true,
                    condition: true,
                    range: true
                },
                itemAdded: function(widget, item) {
                    var colorFormatType = $$get(item, "format.color.type");
                    if ("color" === colorFormatType || "color" === colorFormatType) {
                        var color = item.format.color.color;
                        defined(color) && "transparent" != color && "white" != color && "#fff" != color && "#ffffff" != color || $jaql.resetColor(item)
                    }
                    "range" === colorFormatType && $jaql.resetColor(item), defined(item, "format.color_bkp") && $jaql.resetColor(item), defined(item, "format.members") && delete item.format.members
                },
                visibility: true
            },
            {
                name: 'filters',
                type: 'filters',
                metadata: {
                    types: ['dimensions'],
                    maxitems: -1
                }
            }
        ],
        //  Allow coloring for the value panel
        canColor: function (widget, panel, item) {
            return (panel.name === "Value" );
        },
        // builds a jaql query from the given widget
        buildQuery: function (widget) {

            // building jaql query object from widget metadata 
            var query = { 
                datasource: widget.datasource, 
                format: "json",
                isMaskedResult:true,
                metadata: [] 
            };

            //  Get the metadata panels
            var latItems = widget.metadata.panel("Latitude").items,
                lngItems = widget.metadata.panel("Longitude").items,
                labelItems = widget.metadata.panel("Label").items,
                valItems = widget.metadata.panel("Value").items,
                filterItems = widget.metadata.panel("filters").items;

            //  Do we have all required fields?
            var isValid = (latItems.length>0) && (lngItems.length>0) && (valItems.length>0);
            if (isValid) {

                // push Latitude values
                latItems.forEach(function(item){
                    
                    //  Create a new version of the item
                    var newItem = $$.object.clone(item, true);

                    //  Add a filter to exclude nulls
                    newItem.jaql.filter = {
                        ">=":-90,
                        "<=":90
                    }

                    //  Save to metadata array
                    query.metadata.push(newItem);
                })

                // push longitude values
                lngItems.forEach(function(item){
                    
                    //  Create a new version of the item
                    var newItem = $$.object.clone(item, true);

                    //  Add a filter to exclude nulls
                    newItem.jaql.filter = {
                        ">=":-180,
                        "<=":180
                    }

                    //  Save to metadata array
                    query.metadata.push(newItem);
                })

                // push label Items
                labelItems.forEach(function(item){
                    //  Save to metadata array
                    query.metadata.push(item);
                })

                // push Value Items
                valItems.forEach(function(item){
                    //  Save to metadata array
                    query.metadata.push(item);
                })

                // push widget filter selections
                filterItems.forEach(function(item){
                    
                    //  Create a new version of the item
                    var newItem = $$.object.clone(item, true);

                    //  Specify that this is a filter
                    newItem.panel = "scope";

                    //  Save to metadata array
                    query.metadata.push(newItem);
                })
            }
            
            return query;
        },
        //  Build geojson object from query result
        processResult : function (widget, queryResult) {

            //  Get the Sisense formatter
            var formatter = prism.$injector.get('$filter')('numeric'),
                palette = prism.$ngscope.dashboard.style.palette(),
                defaultTooltipColor = 'black',
                defaultColor = '#F6F6F6';

            //  Get the metadata panels
            var latItems = widget.metadata.panel("Latitude").items,
                lngItems = widget.metadata.panel("Longitude").items,
                labelItems = widget.metadata.panel("Label").items,
                valItems = widget.metadata.panel("Value").items,
                filterItems = widget.metadata.panel("filters").items;

            //  Figure out the column indexes
            var latIdx = 0,
                lngIdx = 1,
                labelIdx = labelItems.length>0 ? 2 : null,
                valueStartIdx = labelItems.length>0 ? 3 : 2,
                numValues = valItems.length;

            //  Init values for the bounding box
            var latMin = queryResult.$$rows[0][latIdx].data,
                latMax = queryResult.$$rows[0][latIdx].data, 
                longMin = queryResult.$$rows[0][lngIdx].data,
                longMax = queryResult.$$rows[0][lngIdx].data;

            //  Figure out min/max colors
            var colorBand = [],
                colorMin,
                colorMax,
                colorSettings = $$get(valItems[0],'format.color', {});
            if (colorSettings.color) {
                colorMax = colorSettings.color;
                colorMin = defaultColor;
                colorBand = getRamp(defaultColor, colorSettings.color, 6);
            } else if (colorSettings.max || colorSettings.min) {
                colorMax = $$get(colorSettings, 'max', defaultColor);
                colorMin = $$get(colorSettings, 'min', defaultColor);
                colorBand = getRamp(colorMin, colorMax, 6);
            } else if (colorSettings.type == "condition"){
                colorSettings.conditions.forEach(function(c){
                    colorBand.push(c.color);
                })
            } else {
                colorMax = palette[0];
                colorMin = palette[1];
                colorBand = palette;
            }
        
            //  Create the outline for a geojson object
            var geojson = {
                type: "FeatureCollection",
                features: [],
            };
            
            //  Loop through each result
            for (i = 0; i < queryResult.$$rows.length; i++) {
                
                //  Get the values from the row
                var lng = queryResult.$$rows[i][lngIdx].data,
                    lat = queryResult.$$rows[i][latIdx].data,
                    label = labelIdx ? queryResult.$$rows[i][labelIdx].text : lat + ', ' + lng,
                    firstValue = queryResult.$$rows[i][valueStartIdx].data,
                    firstColor = queryResult.$$rows[i][valueStartIdx].color ? queryResult.$$rows[i][valueStartIdx].color : $$get(valItems[0],'format.color.color', prism.$ngscope.dashboard.style.palette()[0]);
                
                //  Setup bounding box   
                if (lat > latMax){
                    latMax = lat;
                }
                if (lat < latMin){
                    latMin = lat;
                }
                
                if (lng > longMax){
                    longMax = lng;
                }
                if (lng < longMin){
                    longMin = lng;
                }

                //  Define the point
                var point = {
                    "type": "Feature",
                    "properties": {
                        "id": i,
                        "label": label,
                        "value": firstValue,
                        "color": firstColor,
                        "marker-color": firstColor,
                        "marker-symbol": "default_marker",
                        "marker-size": "medium",
                        "tooltips": {}
                    },
                    "geometry": {
                        "type": "Point",
                        "coordinates": [lng, lat]
                        }
                }

                //  Add all values
                for (var j=0; j<numValues; j++){

                    //  Get the value details
                    var valueKey = valItems[j].jaql.title,
                        valueData = queryResult.$$rows[i][valueStartIdx+j].data,
                        valueMask = $$get(valItems[j],'format.mask', null),
                        valueColor = $$get(valItems[j],'format.color.color', defaultTooltipColor);

                    //  Save to the point
                    point.properties.tooltips[valueKey] = {
                        'value': valueData,
                        'text': formatter(valueData,valueMask),
                        'color': valueColor
                    };

                }

                //  Save to the feature
                geojson.features.push(point);
            }

            //  Define an object containing the new results
            var newResults = {
                geojson: geojson,
                box: [[longMin, latMin], [longMax,latMax]],
                colorRange: {
                    min: colorMin,
                    max: colorMax,
                },
                colors: colorBand
            };
            
            //  return new results
            return newResults;
        }
    },
    render : function (widget, event) {

        //  Function to get the map objects
        function getMap(){

            //  Define the widget id (handle new widgets)
            var widgetId = widget.oid ? widget.oid : 'new';

            //  Find the map
            return $$get(prism.mapboxglwidget, widgetId, null);
        }

        //  Function to get the bottom layer for this map style
        function getBottomLayer(){

            //  Find the selected basemap object
            var selection = settings.basemaps.filter(function(m){
                return (m.value == widget.style.basemap);
            })
            
            //  return the bottom layer from config.js
            return (selection.length>0) ? selection[0].bottomLayer : '';
        }

        //  Function to handle tooltips
        function mapTooltip(event, popup){

            //  Get the map object
            var map = getMap().map;

            // Change the cursor style as a UI indicator.
            map.getCanvas().style.cursor = 'pointer';

            //  Set the coordinates
            var coordinates = event.features[0].geometry.coordinates.slice();

            //  Define HTML to use for this popup
            var props = event.features[0].properties,
                tooltips = JSON.parse(props.tooltips);

            //  Get the label to use
            var label = props.label ? props.label : props.id;
            var htmlDesc = '<div class="mapboxgl-tooltip-container">'
                            + '<div class="mapboxgl-tooltip-label">'
                                + label
                            + '</div>'
                            + '<div class="mapboxgl-tooltip-values">';

            //  Loop through and find all values
            for (key in tooltips){

                //  Make sure this is something we want to show
                var isValid = (tooltips.hasOwnProperty(key));
                if (isValid) {

                    //  Defne html for this value
                    var valueHtml = '<div class="mapboxgl-tooltip-value" style="color: ' + tooltips[key].color + ';">'
                                        + key + ': ' + tooltips[key].text
                                    + '</div>'

                    //  Add to the existing thml
                    htmlDesc = htmlDesc + valueHtml
                }
            }

            //  Close the html divs
            htmlDesc = htmlDesc + '</div></div>';

            // Ensure that if the map is zoomed out such that multiple
            // copies of the feature are visible, the popup appears
            // over the copy being pointed to.
            while (Math.abs(event.lngLat.lng - coordinates[0]) > 180) {
                coordinates[0] += event.lngLat.lng > coordinates[0] ? 360 : -360;
            }

            // Populate the popup and set its coordinates
            // based on the feature found.
            popup.setLngLat(coordinates)
                .setHTML(htmlDesc)
                .addTo(map);
        }

        //  Function to ensure the map has loaded (race condition)
        function checkForMap(widget){

            //  Find the map
            var mapObj = getMap(),
                mapLoaded = widget.options.mapLoaded;

            if (mapLoaded){
                //  Found the map, ready to start
                loadMap(mapObj, widget);
            } else {
                //  Map not loaded
                setTimeout(function(){
                    checkForMap(widget)
                }, 500)
            }
        }

        //  Function to clear and load the map layers
        function loadMap(mapObj, widget){

            //  Get the objects
            var map = mapObj.map,
                control = mapObj.control,
                resetControl = mapObj.resetControl;

            //  define the source ids
            var sourceId = 'elasticubeData',
                clusterLayerId = 'layer-cluster',
                clusterLabelLayerId = 'layer-clusterLabel',
                heatLayerId = 'layer-heatmap',
                circleLayerId = 'layer-circle',
                bottomLayer = getBottomLayer(),
                zoomLevel = widget.style.zoomLevel;

            /***************************************/    
            /*  Remove any existing data/layers    */
            /***************************************/

            //  Remove the old layers, if they were already added
            if (map.getLayer(clusterLayerId)){
                map.removeLayer(clusterLayerId);
            }
            if (map.getLayer(clusterLabelLayerId)){
                map.removeLayer(clusterLabelLayerId);
            }
            if (map.getLayer(heatLayerId)){
                map.removeLayer(heatLayerId);
            }
            if (map.getLayer(circleLayerId)){
                map.removeLayer(circleLayerId);
            }

            //  Remove the old source, if there is one
            if (map.getSource(sourceId)){
                map.removeSource(sourceId);
            }

            /********************************/    
            /*  Add Data Source & Layers    */
            /********************************/

            //  Define the dataset to load
            var data = {
                type: 'geojson',
                data: widget.queryResult.geojson
            }

            //  Special settings when clustering
            if (widget.style.renderType == 'cluster') {
                data.cluster = true;
                data.clusterMaxZoom = zoomLevel;
                data.clusterRadius = 50;
            }

            //  add the data to the map
            map.addSource(sourceId, data);

            //  Check the binning type
            var isHeatmap = (widget.style.renderType == 'heatmap'),
                isCluster = (widget.style.renderType == 'cluster');

            //  What type of map are we rendering
            if (isHeatmap) {

                // add heatmap layer here
                map.addLayer(getLayerHeatmap(sourceId, heatLayerId, widget.queryResult.colors, zoomLevel, widget.style.intensity), bottomLayer);
            } else if (isCluster) {

                // add clustering layers here
                map.addLayer(getLayerCluster(sourceId,clusterLayerId,zoomLevel), bottomLayer);
                map.addLayer(getLayerClusterCount(sourceId,clusterLabelLayerId,zoomLevel), bottomLayer);
            }

            // add circle layer here
            map.addLayer(getLayerCircle(sourceId,circleLayerId,zoomLevel,isCluster), bottomLayer);


            /********************/    
            /*  Map Controls    */
            /********************/

            //  Safely add the control
            map.removeControl(control);
            map.addControl(control, widget.style.controlLocation);
            map.removeControl(resetControl);
            map.addControl(resetControl, 'top-left');
            

            /****************/    
            /*  Tooltips    */
            /****************/

            // Create a popup, but don't add it to the map yet.
            var popup = new mapboxgl.Popup({
                closeButton: false,
                closeOnClick: false
            });

            //  Add handler for tooltips
            map.on(widget.style.tooltipEvent, circleLayerId, function(event){
                mapTooltip(event, popup);
            });

            //  Additional handler for hover tooltips
            if (widget.style.tooltipEvent == "mouseenter") {
                map.on("mouseleave", circleLayerId, function(){
                    map.getCanvas().style.cursor = '';
                    popup.remove();
                });
            }

            /******************/    
            /*  Last Steps    */
            /******************/

            //  Define the fit options
            var fitOptions = {
                maxZoom: settings.zoom.max,
                padding: {
                    top: settings.fitPadding,
                    bottom: settings.fitPadding,
                    left: settings.fitPadding,
                    right: settings.fitPadding
                }
            }

            //  Center the map around the data points
            map.fitBounds(widget.queryResult.box,fitOptions);

            //  Notify Sisense that the widget is done loading (for PDF printing)
            widget.trigger('domready', widget);
        }

        //  Does this widget have any data?
        var hasData = !$.isEmptyObject(widget.queryResult);
        if (hasData) {
            //  Yes! Start the process
            checkForMap(widget);
        }
    },
    destroy : function (widget, args) {},
    options: {
        dashboardFiltersMode: "slice",
        selector: false,
        title: false
    },
    sizing: {
        minHeight: 128, //header
        maxHeight: 2048,
        minWidth: 128,
        maxWidth: 2048,
        height: 320,
        defaultWidth: 512
    }
});

/*************************/
/*  Layering Functions   */
/*************************/


//  Layer for clusters
function getLayerCluster(sourceId,layerId,maxzoom){

    //  Get the current sisense color palette
    var palette = prism.$ngscope.dashboard.style.palette();

    return {
        id: layerId,
        type: "circle",
        source: sourceId,
        minzoom: 0,
        maxzoom: maxzoom,
        filter: ["has", "point_count"],
        paint: {
            // Use step expressions (https://www.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
            // with three steps to implement three types of circles:
            //   * Blue, 20px circles when point count is less than 100
            //   * Yellow, 30px circles when point count is between 100 and 750
            //   * Pink, 40px circles when point count is greater than or equal to 750
            "circle-color": [
                "step",
                ["get", "point_count"],
                palette[2],
                100,
                palette[1],
                750,
                palette[0]
            ],
            "circle-radius": [
                "step",
                ["get", "point_count"],
                20,
                100,
                30,
                750,
                40
            ]
        }
    }
}

//  Layer for cluster labels
function getLayerClusterCount(sourceId, layerId, maxZoom){
    return {
        id: layerId,
        type: "symbol",
        source: sourceId,
        minzoom: 0,
        maxzoom: maxZoom,
        filter: ["has", "point_count"],
        layout: {
            "text-field": "{point_count_abbreviated}",
            "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Bold"],
            "text-size": 12
        }
    };
}

//  Layer for heatmap
function getLayerHeatmap(sourceId,layerId, colors, maxZoom, intensity){

    //  Define outline of the layer
    var layer = {
      id: layerId,
      type: 'heatmap',
      source: sourceId,
      maxzoom: maxZoom,
      paint: {
        // increase weight as diameter breast height increases
        'heatmap-weight': {
          property: 'id',
          type: 'exponential',
          stops: [
            [1, 0],
            [62, 1]
          ]
        },
        // increase intensity as zoom level increases
        'heatmap-intensity': [
            "interpolate",
            ["exponential",2],
            ["zoom"],
            0, 0.01 * intensity,
            maxZoom, 10 * intensity
        ],
        // assign color values be applied to points depending on their density
        'heatmap-color': [
          'interpolate',
          ['linear'],
          ['heatmap-density']
        ],
        // increase radius as zoom increases
        'heatmap-radius': [
            "interpolate",
            ["exponential",1],
            ["zoom"],
            0, 1,
            maxZoom, 10
        ],
        // decrease opacity to transition into the circle layer
        'heatmap-opacity': [
            "interpolate",
            ["linear"],
            ["zoom"],
            maxZoom-2, 1,
            maxZoom, 0.3
        ],
      }
    }
    //  Figure out the ramping options
    //var colorRamp = getRamp(minColor, maxColor, 6);
    for (var i=0; i<colors.length; i++){

        //  Figure out the stop and color
        //var stop = (i+1)/colors.length,
        var stop = logslider(i,colors.length-1),
            stopColor = colors[i];

        //  Save the stop settings to the layer
        layer.paint['heatmap-color'].push(stop);
        layer.paint['heatmap-color'].push(stopColor);
    }

    /*
    0, 'rgba(236,222,239,0)',
    0.2, 'rgb(208,209,230)',
    0.4, 'rgb(166,189,219)',
    0.6, 'rgb(103,169,207)',
    0.8, 'rgb(28,144,153)'
    */

    return layer;
}

//  Bottom layer (circles)
function getLayerCircle(sourceId, layerId, minzoom, isCluster){

    var layer = {
      id: layerId,
      type: 'circle',
      source: sourceId,
      paint: {
        // increase the radius of the circle as the zoom level and dbh value increases
        'circle-radius': {
          property: 'id',
          type: 'exponential',
          stops: [
            [{ zoom: minzoom, value: 1 }, 5],
            [{ zoom: minzoom, value: 62 }, 10],
            [{ zoom: minzoom+3, value: 1 }, 20],
            [{ zoom: minzoom+3, value: 62 }, 50],
          ]
        },
        'circle-color': {
          property: 'color',
          type: 'identity'
        },
        'circle-stroke-color': 'white',
        'circle-stroke-width': 1
      }
    };

    if (isCluster) {
        layer.filter = ["!has", "point_count"];
    } else {
        layer.minzoom = minzoom;
        layer.paint['circle-opacity'] = {
          stops: [
            [minzoom, 0.5],
            [minzoom+1, 1]
          ]
        }
    }

    return layer;
}

/************************/
/*  Utility Functions   */
/************************/

function rgbToHex(r, g, b) {
  return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result
    ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
      }
    : null;
}

// returns an array of startColor, colors between according to steps, and endColor
function getRamp(startColor, endColor, steps) {
  var ramp = [];

  ramp.push(startColor);

  var startColorRgb = hexToRgb(startColor);
  var endColorRgb = hexToRgb(endColor);

  var rInc = Math.round((endColorRgb.r - startColorRgb.r) / (steps+1));
  var gInc = Math.round((endColorRgb.g - startColorRgb.g) / (steps+1));
  var bInc = Math.round((endColorRgb.b - startColorRgb.b) / (steps+1));

  for (var i = 0; i < steps; i++) {
    startColorRgb.r += rInc;
    startColorRgb.g += gInc;
    startColorRgb.b += bInc;

    ramp.push(rgbToHex(startColorRgb.r, startColorRgb.g, startColorRgb.b));
  }
  ramp.push(endColor);

  return ramp;
}

function logslider(position,count) {
  // position will be between 0 and 100
  var minp = 0;
  var maxp = count;

  // The result should be between 100 an 10000000
  var minv = Math.log(20);
  var maxv = Math.log(99.9);

  // calculate adjustment factor
  var scale = (maxv-minv) / (maxp-minp);

  return (Math.exp(minv + scale*(position-minp)) / 100);
}

